These are my notes for getting ZFS root on Debian under LUKS encryption, using backports ZFS.

Its not really clean but should work as a guide.

# Caveats

* This will use full disk encryption on a single disk.
* Do not use this guide for dual booting
* This assumes old BIOS style booting, and not EFI booting.
* This assumes booting from a Debian Live CD. You may need to use the non-free firmware to establish an Internet connection.

This guide is derived from the ZFSOnLinux repo:

* [Debian Stretch Root on ZFS](https://github.com/zfsonlinux/zfs/wiki/Debian-Stretch-Root-on-ZFS)
* [Ubuntu 18.04 Root on ZFS](https://github.com/zfsonlinux/zfs/wiki/Ubuntu-18.04-Root-on-ZFS)


# Boot from Live CD

Pop in the CD/DVD or USB stick, and boot. Establish a network connection. Open a terminal to install SSH. Then on a different computer SSH into it and do your work.

# Install Open-SSH on Live CD

    $ sudo apt install openssh-server
    $ sudo sed -i "s/#PasswordAuthentication yes/PasswordAuthentication yes/g" \
          /etc/ssh/sshd_config
    $ sudo service ssh restart

Also make sure that your target system is connected to the network and the internet

## SSH into the Live CD environment

    matthew@matt-tower:~$ ssh user@10.0.0.2

## Become root

    user@debian:~$ sudo -i

## Update Debian sources to include main, contrib, and non-free repos

    root@debian:~# nano /etc/apt/sources.list

Add the following:

    deb http://ftp.debian.org/debian stretch main contrib non-free
    deb http://ftp.debian.org/debian stretch-backports main contrib non-free

## Determine your HDD or SSD device serial number.

    root@debian:~# ls -la /dev/disk/by-id/
    total 0
    drwxr-xr-x 2 root root 280 Sep  8 02:31 .
    drwxr-xr-x 7 root root 140 Sep  8 02:31 ..
    lrwxrwxrwx 1 root root   9 Sep  8 02:31 ata-TSSTcorp_CDDVDW_SN-208AB_R8JT6GCC7003YW -> ../../sr0
    lrwxrwxrwx 1 root root   9 Sep  8 02:31 ata-WDC_WD10JPLX-00MBPT0_JR1000BN1VHLHE -> ../../sda
    lrwxrwxrwx 1 root root  10 Sep  8 02:31 ata-WDC_WD10JPLX-00MBPT0_JR1000BN1VHLHE-part1 -> ../../sda1
    lrwxrwxrwx 1 root root  10 Sep  8 02:31 ata-WDC_WD10JPLX-00MBPT0_JR1000BN1VHLHE-part2 -> ../../sda2
    lrwxrwxrwx 1 root root  10 Sep  8 02:31 ata-WDC_WD10JPLX-00MBPT0_JR1000BN1VHLHE-part4 -> ../../sda4
    lrwxrwxrwx 1 root root   9 Sep  8 02:31 usb-Sony_Storage_Media_5A08042202076-0:0 -> ../../sdb
    lrwxrwxrwx 1 root root  10 Sep  8 02:31 usb-Sony_Storage_Media_5A08042202076-0:0-part1 -> ../../sdb1
    lrwxrwxrwx 1 root root  10 Sep  8 02:31 usb-Sony_Storage_Media_5A08042202076-0:0-part2 -> ../../sdb2
    lrwxrwxrwx 1 root root   9 Sep  8 02:31 wwn-0x5000cca8d8da23b3 -> ../../sda
    lrwxrwxrwx 1 root root  10 Sep  8 02:31 wwn-0x5000cca8d8da23b3-part1 -> ../../sda1
    lrwxrwxrwx 1 root root  10 Sep  8 02:31 wwn-0x5000cca8d8da23b3-part2 -> ../../sda2
    lrwxrwxrwx 1 root root  10 Sep  8 02:31 wwn-0x5000cca8d8da23b3-part4 -> ../../sda4

In my case, it is `wwn-0x5000cca8d8da23b3`

## Zap partition table

    root@debian:~# sgdisk --zap-all /dev/disk/by-id/wwn-0x5000cca8d8da23b3
    GPT data structures destroyed! You may now partition the disk using fdisk or
    other utilities.

## Create partition table

Partition scheme will be a small BIOS partition, ext2 /boot partition, and a LUKS partition where ZFS will be placed

### Create Bios partition

    root@debian:~# sgdisk -a1 -n2:34:2047 -t2:EF02 /dev/disk/by-id/wwn-0x5000cca8d8da23b3 
    Creating new GPT entries.
    The operation has completed successfully.
    root@debian:~# lsblk -fs
    NAME  FSTYPE  LABEL               UUID                                 MOUNTPOINT
    loop0 squashf                                                          /lib/live/
    sda2                                                                   
    └─sda                                                                  
    sdb1  iso9660 d-live nf 9.5.0 xf amd64
    │                                 2018-07-14-17-58-34-00               /lib/live/
    └─sdb iso9660 d-live nf 9.5.0 xf amd64
                                      2018-07-14-17-58-34-00               
    sdb2  vfat    d-live nf 9.5.0 xf amd64
    │                                 7E13-12F7                            
    └─sdb iso9660 d-live nf 9.5.0 xf amd64
                                      2018-07-14-17-58-34-00               
    sr0                                                                    

### Create /boot partition

    root@debian:~# sgdisk -n4:0:+512M -t4:8300 /dev/disk/by-id/wwn-0x5000cca8d8da23b3
    The operation has completed successfully.

### Create partition where LUKS will reside

    root@debian:~# sgdisk -n1:0:0 -t1:8300 /dev/disk/by-id/wwn-0x5000cca8d8da23b3
    The operation has completed successfully.

## Install needed packages to live environment.

I'm installing the ZFS package from backports.

Building the ZFS module *will* take some time. Grab a coffee.

    root@debian:~# apt update
    root@debian:~# apt install --yes debootstrap gdisk dpkg-dev linux-headers-$(uname -r) cryptsetup
    root@debian:~# apt install -t stretch-backports zfs-dkms

## Activate the ZFS module in the live environment

    root@debian:~# modprobe zfs

## Create Encrypted Partition and activate.

I'm calling it `luks1` but you can call it anything you want.

    root@debian:~# cryptsetup luksFormat -c aes-xts-plain64 -s 256 -h sha256 /dev/disk/by-id/wwn-0x5000cca8d8da23b3-part1
    root@debian:~# cryptsetup luksOpen /dev/disk/by-id/wwn-0x5000cca8d8da23b3-part1 luks1

## Create ZFS pool.

I'm calling it `rpool`, but you can call it anything you want.

Note: `feature@large_dnode=disabled` is required if using the ZFS version from Debian backports due to GRUB incompatibilities

    root@debian:~# zpool create -o ashift=12 -O atime=off -O canmount=off -O compression=lz4 -O normalization=formD -O xattr=sa -O mountpoint=/ -R /mnt -o feature@large_dnode=disabled rpool /dev/mapper/luks1
    root@debian:~# zfs create -o canmount=off -o mountpoint=none rpool/ROOT
    root@debian:~# zfs create -o canmount=noauto -o mountpoint=/ rpool/ROOT/debian
    root@debian:~# zfs mount rpool/ROOT/debian
    root@debian:~# zpool set bootfs=rpool/ROOT/debian rpool
    
## Create filesystem pools for segregation

    root@debian:~# zfs create -o setuid=off rpool/home
    root@debian:~# zfs create -o mountpoint=/root rpool/home/root
    root@debian:~# zfs create -o canmount=off -o setuid=off -o exec=off rpool/var
    root@debian:~# zfs create -o com.sun:auto-snapshot=false rpool/var/cache
    root@debian:~# zfs create rpool/var/log
    root@debian:~# zfs create rpool/var/spool
    root@debian:~# zfs create -o com.sun:auto-snapshot=false -o exec=on  rpool/var/tmp

## Create boot partition (ext2)

    root@debian:~# mke2fs -t ext2 /dev/disk/by-id/wwn-0x5000cca8d8da23b3-part4
    root@debian:~# mkdir /mnt/boot
    root@debian:~# mount /dev/disk/by-id/wwn-0x5000cca8d8da23b3-part4 /mnt/boot

## Verify

    root@debian:~# lsblk
    NAME      MAJ:MIN RM   SIZE RO TYPE  MOUNTPOINT
    loop0       7:0    0     2G  1 loop  /lib/live/mount/rootfs/filesystem.squashfs
    sda         8:0    0 931.5G  0 disk  
    ├─sda1      8:1    0   931G  0 part  
    │ └─luks1 254:0    0   931G  0 crypt 
    ├─sda2      8:2    0  1007K  0 part  
    └─sda4      8:4    0   512M  0 part  /mnt/boot
    sdb         8:16   1   7.5G  0 disk  
    ├─sdb1      8:17   1   2.1G  0 part  /lib/live/mount/medium
    └─sdb2      8:18   1   416K  0 part  
    sr0        11:0    1  1024M  0 rom   

# Install Debian

    root@debian:~# chmod 1777 /mnt/var/tmp
    root@debian:~# debootstrap stretch /mnt && zfs set devices=off rpool

This will take time. Best to go grab another coffee. You will see this when complete

    I: Base system installed successfully.

# Configure your system

## Keymap/Keyboard

I use a non-standard keyboard layout. I need to configure it

    root@debian:~# cp /etc/default/keyboard /mnt/etc/default/keyboard
    root@debian:~# nano /mnt/etc/default/keyboard

## Host name

    root@debian:~# echo HOSTNAME > /mnt/etc/hostname
    root@debian:~# echo "127.0.1.1       HOSTNAME" >> /mnt/etc/hosts

# Chroot into install for further configs
	   
    root@debian:~# mount --rbind /dev /mnt/dev
    root@debian:~# mount --rbind /proc /mnt/proc
    root@debian:~# mount --rbind /sys /mnt/sys
    root@debian:~# chroot /mnt /bin/bash --login

# Install necessary apps

## Update sources list and Configure

Add the following to `/etc/apt/sources.list`. Its recommended you pick an appropriate mirror for your region (see the [Debian Mirror List](https://www.debian.org/mirror/list)):

    #Main Debian Repo
    deb http://deb.debian.org/debian stretch main contrib non-free
    deb-src http://deb.debian.org/debian stretch main contrib non-free

    #Debian Updates Repo
    deb http://deb.debian.org/debian stretch-updates main contrib non-free
    deb-src http://deb.debian.org/debian stretch-updates main contrib non-free

    #Debian Security Repo
    deb http://security.debian.org/debian-security/ stretch/updates main contrib non-free
    deb-src http://security.debian.org/debian-security/ stretch/updates main contrib non-free

    #Debian Backports Repo
    deb http://ftp.debian.org/debian stretch-backports main contrib non-free
    deb-src http://ftp.debian.org/debian stretch-backports main contrib non-free

Then execute these commands

    root@debian:/# ln -s /proc/self/mounts /etc/mtab
    root@debian:/# apt update

## Configure Locales and Timezone

    root@debian:/# apt install locales
    root@debian:/# dpkg-reconfigure locales
    root@debian:/# dpkg-reconfigure tzdata

## Install necessary apps for ZFS functionality. Also a good idea to install firmwares, microcodes, etc.

    root@debian:/# apt install gdisk linux-headers-$(uname -r) linux-image-amd64 dpkg-dev cryptsetup
    root@debian:/# apt install -t stretch-backports zfs-dkms zfs-initramfs
    root@debian:/# apt install firmware-linux                       
    root@debian:/# apt install iwlwifi

# Update fstab

    root@debian:/# echo UUID=$(blkid -s UUID -o value /dev/disk/by-id/wwn-0x5000cca8d8da23b3-part4) /boot ext2 defaults 0 2 >> /etc/fstab
    root@debian:/# echo luks1 UUID=$(blkid -s UUID -o value /dev/disk/by-id/wwn-0x5000cca8d8da23b3-part1) none luks,discard,initramfs > /etc/crypttab

# Install Grub

    root@debian:/# apt install grub-pc

# Set Root Password

    root@debian:/# passwd

# Configure Grub

After this command, if you see `zfs` as your output, you can continue. Otherwise you need to fix it.

    root@debian:/# grub-probe /
    zfs

    root@debian:/# apt install grub-pc
    root@debian:/# apt install grub-pc --reinstall

#Update Initramfs

    root@debian:/# update-initramfs -u -k all

# Configure Grub for debugging

    root@debian:/# nano /etc/default/grub
        Remove quiet from: GRUB_CMDLINE_LINUX_DEFAULT
        Uncomment: GRUB_TERMINAL=console
        Save and quit.
    root@debian:/# update-grub

# Install Grub on HDD bootsector

If you see the last line below, you are set.

    root@debian:/# grub-install /dev/disk/by-id/wwn-0x5000cca8d8da23b3
    Installing for i386-pc platform.
    Installation finished. No error reported.

# Verify ZFS module is present

    root@debian:/# ls /boot/grub/*/zfs.mod
    /boot/grub/i386-pc/zfs.mod

# Tasksel for basic tools (SSH, Laptop tools)

Select SSH, Laptop tools, standard system utilities. You *can* pick a desktop environment, but at this point it can be risky.

    root@debian:/# tasksel

# Take a snapshot

    root@debian:/# zfs snapshot rpool/ROOT/debian@install

# Prep for reboot and reboot

Exit the `chroot` environment

    root@debian:/# exit

Unmount the zpool and reboot

    root@debian:~# mount | grep -v zfs | tac | awk '/\/mnt/ {print $3}' | xargs -i{} umount -lf {}
    root@debian:~# zpool export rpool
    root@debian:~# exit
    logout
    user@debian:~$ sudo reboot

# After reboot, verify that you can actually boot!

Once booted, verify an internet connection. If no internet connection, establish one (the hard way if necessary)

## Create User

    zfs create rpool/home/YOURUSERNAME
    adduser YOURUSERNAME
    cp -a /etc/skel/.[!.]* /home/YOURUSERNAME
    chown -R YOURUSERNAME:YOURUSERNAME /home/YOURUSERNAME

Make this new user an admin

    usermod -a -G audio,cdrom,dip,floppy,netdev,plugdev,sudo,video YOURUSERNAME

## Configure Swap

    # zfs create -V 4G -b $(getconf PAGESIZE) -o compression=zle \
      -o logbias=throughput -o sync=always \
      -o primarycache=metadata -o secondarycache=none \
      -o com.sun:auto-snapshot=false rpool/swap

    # mkswap -f /dev/zvol/rpool/swap
    # echo /dev/zvol/rpool/swap none swap defaults 0 0 >> /etc/fstab
    
    # swapon -av
    
## Upgrade the minimal system

    # apt dist-upgrade --yes
    
## Disable log compression

    # for file in /etc/logrotate.d/* ; do
        if grep -Eq "(^|[^#y])compress" "$file" ; then
            
            sed -i -r "s/(^|[^#y])(compress)/\1#\2/" "$file"
        fi
    done

# Verify snapshots and zpool

    sudo zfs list -t snapshot
    sudo zfs list
    lsblk
    lsblk -t
    mount

# Enjoy!